import {Article} from '@prisma/client'
import Showdown from 'showdown'
import {POST_BATCH_SIZE} from '../data/constants'
import {articlePreviewCached} from '../handlers/cache'

export default async (_skip:number) => {
	const converter = new Showdown.Converter()
	const posts:Article[] = await articlePreviewCached(_skip)
	if (posts.length === 0) {
		return '<p>You\'ve reached the end of our content...</p>'
	}
	const rxp = /!\[.*?\]\(.*?\)/g //match all substrings that match the shape ![]() (A markdown image link)
	let content = ''
	for (const article of posts) {
		article.content = article.content.replace(rxp, '')
		const html = converter.makeHtml(article.content)
		content += /*html*/`
        <a class="articleWrapper" href="/articles/${article.id}">
            <div class="article">
                <div class="articleContainer">
                    ${html}
                </div>
                ${article.previewIsSpoiler ? '<div class="spoiler">' : ''}
                    <img src="${article.previewImageUri}"/>
                ${article.previewIsSpoiler ? '</div>' : ''}
            </div>
        </a>
        `
	}
	content += /*html*/`
		<div hx-get="/preview/${_skip + POST_BATCH_SIZE}" hx-trigger="intersect"></div>
	`
	return content
}
