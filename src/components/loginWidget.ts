import {Request} from 'express'

export default (req:Request) => {
	return /*html*/`
		<div class="login">
			<form class="styledForm" id="loginWidget" hx-post="/api/user/login?returnURI=${req.query.returnURI}" hx-trigger="submit">
				<label for="name">Username</label>
				<input type="text" id="name" name="name" class="noValidate"></input>
				<label for="password">Password</label>
				<input type="password" id="password" name="password" class="noValidate"></input>
				<input type="submit" value="Login" id="loginFormSubmit" class="styledButton noValidate"></input>
			</form>
		</div>
	`
}
