/* eslint-disable no-useless-escape */
export default () => {
	return /*html*/`
		<div class="registrationForm">
			<script type="text/javascript">
				const requirements = function() {
					const defaultPattern = /(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*_+;',.:~]).{8,}/;
					const lowerRxp=/(?=.*[a-z])/;
					const upperRxp=/(?=.*[A-Z])/;
					const numberRxp=/(?=.*\\d)/;
					const specialRxp=/(?=.*[!@#$%^&*_+;',.:~])/
					var form = document.getElementById('registration_form');
					var reqs = document.getElementById('passwordRequirements')
					const cross = "❌";
					const check = "✔️";
					if(defaultPattern.test(form.password.value)) {
						document.getElementById('passwordRequirements').classList.add('hide')
					} else {
						document.getElementById('passwordRequirements').classList.remove('hide')
						const password = form.password.value;
						const lengthBullet = document.getElementById("lengthCheck");
						const lowerBullet = document.getElementById("lowerCheck");
						const upperBullet = document.getElementById("upperCheck");
						const numberBullet = document.getElementById("numberCheck");
						const specialBullet = document.getElementById("specialCheck");
						lengthBullet.innerText = password.length >= 8 ? check : cross;
						lowerBullet.innerText = lowerRxp.test(password) ? check : cross;
						upperBullet.innerText = upperRxp.test(password) ? check : cross;
						numberBullet.innerText = numberRxp.test(password) ? check : cross;
						specialBullet.innerText = specialRxp.test(password) ? check : cross;
					}
				}
				const check = function() {
					var form = document.getElementById('registration_form');
					if (form.password.value == form.password_confirm.value) {
						form.password_confirm.setCustomValidity("");
					} else {
						form.password_confirm.setCustomValidity("Does not match.");
					}
				}
				const synchronize = function () {
					var form = document.getElementById('registration_form');
					const list = 
					form.display_name.value = form.username.value
												.split("_")
												.map((v) => {return v.charAt(0).toUpperCase() + v.slice(1)})
												.join(" ");
				}
			</script>
			<form action="/api/user/register" method="post" id="registration_form" class="styledForm">
				<label for="username">Username</label>
				<input type="text" id="username" name="username" required="required" pattern="[a-z0-9_]{1,16}" onkeyup="synchronize()"></input>
				<div class="requirementHelp">Usernames must be unique, with a length between 1 and 16 characters long, and may only contain lower-case letters, numbers, and underscores</div>
				<button class="checkAvailability" hx-trigger="click" hx-swap="innerHtml" hx-get="/api/user/checkUsername" hx-include="#username" hx-target="#availabilityLabel">Check Username Availability</button>
				<div id="availabilityLabel"></div>
				<label for="display_name">Display Name</label>
				<input type="text" id="display_name" name="display_name" required="required" pattern="[a-zA-Z0-9_ ]{1,32}"></input>
				<div class="requirementHelp">Display Names can be a maximum of 32 characters long, and may only contain letters, numbers, spaces, and underscores.</div>
				<label for="password">Password</label>
				<input class="" id="password" type="password" name="password" required="required" pattern="(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*_+;',.:~]).{8,}" onkeyup="requirements()"></input>
				<div class="hide" id="passwordRequirements">	
					<p>a password must contain all of the following:</p>
					<ul id="passwordRequirementsListDefault">
						<li><span id="lengthCheck">❌</span>At least 8 characters.</li>
						<li><span id="lowerCheck">❌</span>At least 1 lower-case letter.</li>
						<li><span id="upperCheck">❌</span>At least 1 upper-case letter.</li>
						<li><span id="numberCheck">❌</span>At least 1 number.</li>
						<li><span id="specialCheck">❌</span>At least one special character from the following list: !@#$%^&*_+;',.:~ </li>
					</ul>
				</div>
				<label for="password_confirm">Confirm Password</label>
				<input class="" id="password_confirm" type="password" name="password_confirm" required="required" onkeyup="check()"></input>
				<input type="submit"></input>
			</form>
		</div>
	`
}
