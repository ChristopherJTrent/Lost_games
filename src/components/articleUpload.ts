export default () => {
	/*  id Int @id @default(sequence())
  title String
  content String
  publishTimeEpoch Int @default(0) //1-JAN-1970
  previewImageUri String
  previewIsSpoiler Boolean @default(false)
  author User @relation(fields: [authorId], references: [id])
  authorId Int*/
	return /*html*/`
	<div>
	<form id="articleUploadForm" class="styledForm">
		<input type="text" id="title" name="title"></input>	
		<textarea id="content" name="content"></textarea>
		
		<input type="checkbox" id="spoiler" name="spoiler"></input>
	</div>
	`
}
