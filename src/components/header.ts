import {Request} from 'express'
import authentication from '../handlers/authentication'
import {userCached} from '../handlers/cache'

const links = [['Home', '/'], ['Articles', '/articles']]

async function loggedIn(req:Request) {
	const user = await userCached(req)
	if (user === undefined) {
		return ''
	}
	return /*html*/`<li class="userBlock" hx-get="/user/logout" hx-trigger="click" hx-swap="innerHtml"/><p>${user.displayname}</p><img class="smallIcon" src="${user.iconUri}"/></li>`
}

export function loggedOut(req:Request) {
	return /*html*/`<li class="userBlock"><a href="/user/login?returnURI=${encodeURIComponent(req.baseUrl)}">Login</a></li>`
}

async function navbar(req:Request) {
	return /*html */`
    <div class="navbar"> 
        <ul class="navigation">
            <li class="logo"></li>
            ${links.map((v) => { return `<li><a class="navlink" href="${v[1]}">${v[0]}</a></li>` }).join('\n')}
			${await authentication(req)?await loggedIn(req):loggedOut(req)}
        </ul>
	</div>
`
}

export async function header(req:Request) {

	return /*html*/`
	<div class="header"> 
		${await navbar(req)}
	</div>
	`
}
