import bcrypt from 'bcrypt'
import {Request} from 'express'
import {prisma} from '../globals'

const saltRounds = 10

export default async (req:Request):Promise<boolean> => {
	if (req.body === null) { return false }
	const name:string = req.body.username
	if (await prisma.user.findUnique({where: {name: name}}) !== null) { return false }
	const displayName:string = req.body.display_name
	let iconUri:string | undefined = req.body.iconUri
	const password:string = req.body.password
	const passwordHash = await bcrypt.hash(password, saltRounds)
	if (iconUri === undefined) { iconUri = '/image/defaultIcon.png' }
	await prisma.user.create({data: {
		name: name,
		displayname: displayName,
		iconUri: iconUri,
		passwordHash: passwordHash,
	}})
	return true
}
