import {Article, User} from '@prisma/client'
import {Request} from 'express'
import {decode} from 'jsonwebtoken'
import {POST_BATCH_SIZE} from '../data/constants'
import {prisma} from '../globals'

//TODO: Rewrite cache to work as proper middleware

type cacheEntry<T> = {
    content: T,
    expiry: Date
}

const previewTruncLengthMD = 200
const timeOffsetSeconds = 86400 //1 day in seconds
// eslint-disable-next-line @typescript-eslint/no-magic-numbers
const articleOffsetSeconds = timeOffsetSeconds * 7 //1 week in seconds

let previewCache: Record<number, cacheEntry<Article[]>> = {}
let articleCache: Record<number, cacheEntry<Article>> = {}
const userCache: Record<string, cacheEntry<User>> = {}

/**
 * checks for an unexpired cache hit on a given skip value
*/
export async function articlePreviewCached(_skip: number): Promise<Article[]> {
	if (previewCache[_skip] !== undefined) {
		const entry = previewCache[_skip]
		if (entry.expiry.getTime() > Date.now()) {
			return entry.content
		}
		return cachePreview(_skip)

	}  return cachePreview(_skip)
}

export async function articleCached(_id: number): Promise<Article> {
	if (articleCache[_id] !== undefined) {
		const entry = articleCache[_id]
		if (entry.expiry.getTime() > Date.now()) {
			return entry.content
		}
		return cacheArticle(_id)

	}
	return cacheArticle(_id)

}

export async function userCached(_name:string|Request):Promise<User | undefined> {
	let __name = ''
	if (typeof _name !== 'string') {
		const jwt = _name.cookies.authJWT
		const decoded = decode(jwt)
		if (decoded === null || typeof decoded === 'string') {
			return undefined
		}
		__name = decoded.username
	} else {
		__name = _name
	}
	if (__name in userCache && userCache[__name].expiry > new Date()) {
		return userCache[__name].content
	}
	try {
		const user = await prisma.user.findUniqueOrThrow({where: {name: __name}})
		const exp = new Date()
		exp.setSeconds(exp.getSeconds() + timeOffsetSeconds)
		userCache[__name] = {content: user, expiry: exp}
		return user
	} catch (e) {
		return undefined
	}
}

async function cachePreview(_skip: number): Promise<Article[]> {
	const updatedExpiry = new Date()
	updatedExpiry.setSeconds(updatedExpiry.getSeconds() + timeOffsetSeconds)
	const posts: Article[] = await prisma.article.findMany({
		skip: _skip,
		take: POST_BATCH_SIZE,
	})
	posts.map((v) => {
		v.content = v.content.length > previewTruncLengthMD ? v.content.substring(0, previewTruncLengthMD) + '...' : v.content
	})
	previewCache[_skip] = {content: posts, expiry: updatedExpiry}
	return posts
}

async function cacheArticle(_id: number): Promise<Article> {
	const updatedExpiry = new Date()
	updatedExpiry.setSeconds(updatedExpiry.getSeconds() + articleOffsetSeconds)
	const article: Article = await prisma.article.findFirstOrThrow({
		where: {
			id: _id,
		},
	})
	articleCache[_id] = {content: article, expiry: updatedExpiry}
	return article
}

export async function invalidateCache() {
	previewCache = {}
	articleCache = {}
}
