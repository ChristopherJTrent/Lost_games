import {Request} from 'express'
import {verify} from 'jsonwebtoken'
import {userCached} from './cache'

export default async (req:Request):Promise<boolean> => {
	if (req.cookies.authJWT === undefined || req.cookies.xsrf === undefined) { return false }
	const token = req.cookies.authJWT
	const xsrf = req.cookies.xsrf
	const validatedToken = verify(token, process.env.JWT_SECRET ?? '')
	try {
		if (typeof validatedToken === 'string' || validatedToken.XSRF === undefined || validatedToken.username === undefined) {
			return false
		}
		if (xsrf === validatedToken.XSRF) {
			return userCached(validatedToken.username) !== undefined
		}
		return false
	} catch (e) {
		return false
	}
}
