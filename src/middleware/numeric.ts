import {NextFunction, Request, Response} from 'express'
import {INVALID_REQUEST} from '../data/constants'

export default (ident:string) => async (req:Request, res:Response, next:NextFunction) => {
	const n = parseInt(req.params[ident])
	if (!isNaN(n) && isFinite(n) &&(n >= 0)) {
		next()
	} else {
		res.sendStatus(INVALID_REQUEST)
	}
}
