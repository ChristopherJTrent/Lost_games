import {NextFunction, Request, Response} from 'express'

const redirectDomains = ['favicon.ico', 'image', 'css', 'html']

export default () => async (req: Request, res: Response, next: NextFunction) => {
	const base = req.originalUrl.split('/')[1]
	if (redirectDomains.includes(base)) {
		res.contentType(req.originalUrl.split('.')[1])
		res.setHeader('Access-Control-Allow-Origin', '')
		res.redirect('https://dtkzfll605gir.cloudfront.net' + req.originalUrl.replace(/\?.*/, ''))
		res.send()
	} else {
		next()
	}
}
