import {Request, Response} from 'express'
import {article} from '../../components/article'
import root from '../../components/root'
import {NOT_FOUND} from '../../data/constants'
import numeric from '../../middleware/numeric'

export const get = [numeric('id'),
	async (req:Request, res:Response) => {
		try {
			res.send(await root(await article(parseInt(req.params.id)), req))
		} catch (e) {
			res.sendStatus(NOT_FOUND)
		}
	}]
