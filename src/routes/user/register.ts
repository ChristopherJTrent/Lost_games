import {Request, Response} from 'express'
import registrationWidget from '../../components/registrationWidget'
import root from '../../components/root'

export const get = async (req:Request, res:Response) => {
	res.send(await root(registrationWidget, req))
}
