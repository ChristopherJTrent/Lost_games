import bcrypt from 'bcrypt'
import {Request, Response} from 'express'
import {sign} from 'jsonwebtoken'
import {JWTEXPIRATION} from '../../../data/constants'
import redirect from '../../../handlers/redirect'
import userLogin from '../../../handlers/userLogin'

//TODO: Spin this into /user/login@POST

export const post = async (req:Request, res:Response) => {
	const success = await userLogin(req)
	if (success) {
		const XSRFToken = await bcrypt.genSalt()
		const token = sign({
			XSRF: XSRFToken,
			username: req.body.name,
		},
		process.env.JWT_SECRET ?? '',
		{
			expiresIn: JWTEXPIRATION * 1000,
		})
		res.cookie('authJWT', token, {maxAge: JWTEXPIRATION * 1000, httpOnly: true})
		res.cookie('xsrf', XSRFToken, {maxAge: JWTEXPIRATION * 1000, httpOnly: true})
		redirect(res, req)
	} else {
		redirect(res, '/')
	}
}
