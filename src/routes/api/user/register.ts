import {Request, Response} from 'express'
import userRegistration from '../../../handlers/userRegistration'

//TODO: Spin this into /user/logout@POST

export const post = async (req:Request, res:Response) => {
	if (await userRegistration(req)) {
		res.redirect('/user/login')
	}
}
